<?php
require("RollingCurl.php");

class Download {
    private $patch;
    private $isValid = TRUE;

    function __construct($urls, $patch, $single_mode = FALSE) {
        $this->patch = $patch. "/". date("d-m-Y", time())."/";
        $rc = new RollingCurl(array($this, 'request_callback'));
        if($single_mode != TRUE) {
            $rc->window_size = 2;
            foreach ($urls as $url) {
                $request = new RollingCurlRequest($url);
                $rc->add($request);
            }
            $rc->execute();
        } else {
            foreach ($urls as $url) {
                $rc->request($url);
                $rc->execute();
            }
        }
    }
    
    private function parseFileName($image_name) {
        $arr      = explode("/", $image_name);
        $img_name = $arr[(count($arr) - 1)];
        return $img_name;
    }
    
     function request_callback($response, $info, $request) {
        $fileName = $this->parseFileName($info['url']);
        
        if (!$this->trainingDir()) {
            echo "Директория $this->patch не может быть создана\r\n";
            $this->isValid = FALSE;
        }

        if ($info['size_download'] > 0) {
            if (file_put_contents("$this->patch$fileName", $response)){
                echo "Файл $fileName загружен,  размер ".$this->filesizeFormat($info['size_download'])." \r\n";
                $this->clearXml("$this->patch$fileName");
                $this->xmlValid("$this->patch$fileName");
            } else {
                echo "Не удается записать файл $this->patch$fileName \r\n";
                $this->isValid = FALSE;
            }
        } else {
            echo "Файл $fileName не может быть загружен, код $info[http_code]\r\n";
            $this->isValid = FALSE;
        }
    }
    
    private function trainingDir() {
        if (!file_exists($this->patch)) {
            return mkdir($this->patch, 0750, TRUE);
        } else {
            return TRUE;
        }
    }
    
    private function clearXml($file) {
        $data = file_get_contents($file);
        $data = preg_replace('/[^\x20-\x7E\sА-Яа-яёЁ–«»—]/u', '', $data); //не произносить в слух, можно вызвать Сатану
        $data = preg_replace ("/\s{1,}<\//u" , '</' , $data); 
        $data = preg_replace ("/[ ]{2,}/u" , ' ' , $data); 
        file_put_contents($file, $data);
    }
    
    private function xmlValid($file) {
        libxml_use_internal_errors(true);
        $sxe = simplexml_load_file($file);
        if (!$sxe) {
            $this->isValid = FALSE;
            echo "Ошибка загрузки XML\r\n";
            foreach(libxml_get_errors() as $error) {
                echo "Строка $error->line, ". $error->message."\r\n";
            }
        }
    }

    private function filesizeFormat($filesize) {
        $formats = array('Б', 'КБ', 'МБ', 'ГБ', 'ТБ'); // варианты размера файла
        $format  = 0; // формат размера по-умолчанию
        // прогоняем цикл
        while ($filesize > 1024 && count($formats) != ++$format) {
            $filesize = round($filesize / 1024, 2);
        }
        $formats[] = 'ТБ';

        return $filesize . $formats[$format];
    }
    
    public function run() {
        return $this->isValid;
    }
}